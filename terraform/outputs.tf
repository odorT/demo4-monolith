output "region" {
  description = "AWS region"
  value       = var.region
}

output "cluster_name" {
  description = "Kubernetes Cluster Name"
  value       = local.cluster_name
}

output "rds-endpoint" {
  description = "The connection url endpoint for database"
  value       = aws_db_instance.demo4-monolith-rds.address
}

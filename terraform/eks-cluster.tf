locals {
  cluster_name = "demo4-cluster-monolith"
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = var.cluster_version
  subnets         = module.vpc.private_subnets

  vpc_id = module.vpc.vpc_id

  workers_group_defaults = {
    root_volume_type = "gp2"
  }

  worker_groups = [
    {
      name          = var.worker-name1
      instance_type = var.worker-type1
      # additional_userdata           = "echo foo bar"
      asg_desired_capacity          = var.worker-asg-capacity1
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_one.id]
    },
    {
      name          = var.worker-name2
      instance_type = var.worker-type2
      # additional_userdata           = "echo foo bar"
      asg_desired_capacity          = var.worker-asg-capacity2
      additional_security_group_ids = [aws_security_group.worker_group_mgmt_two.id]
    },
  ]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

resource "aws_security_group" "worker_group_mgmt_one" {
  name_prefix = "worker_group_mgmt_one"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = var.ingress-cidr-blocks
  }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name_prefix = "worker_group_mgmt_two"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = var.ingress-cidr-blocks
  }
}

resource "aws_security_group" "all_worker_mgmt" {
  name_prefix = "all_worker_management"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = var.ingress-cidr-blocks
  }
}

resource "aws_security_group" "demo4-monolith" {
  name        = "demo4-monolith-rds-sg"
  description = "SG for RDS instances"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "Ingress rules for RDS instances"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"

    cidr_blocks = var.ingress-cidr-blocks
  }

  egress {
    description = "Egress rule for RDS instances"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }
}

variable "region" {
  default     = ""
}


### VPC variables

variable "vpc-cidr" {
  default = ""
}

variable "vpc-priv-subnets" {
  default = ""
}

variable "vpc-pub-subnets" {
  default = ""
}


### EKS Cluster variables

variable "cluster_version" {
  default = ""
}

## worker group variables 1

variable "worker-name1" {
  default = ""
}

variable "worker-type1" {
  default = ""
}

variable "worker-asg-capacity1" {
  type = number
  default = 0
}

## worker group variables 2

variable "worker-name2" {
  default = ""
}

variable "worker-type2" {
  default = ""
}

variable "worker-asg-capacity2" {
  type = number
  default = 0
}


#### RDS variables

variable "rds-type" {
  default = ""
}

variable "rds-storage" {
  type = number
  default = 0
}

variable "rds-storage-max" {
  type = number
  default = 0
}

variable "rds-storage_type" {
  default = ""
}

variable "rds-engine" {
  default = ""
}

variable "rds-engine_version" {
  default = ""
}

variable "identifier" {
  default = ""
}

variable "db_name" {
  default = ""
}

variable "db_user" {
  default = ""
}

variable "db_pass" {
  default = ""
}

variable "db_port" {
  default = ""
}


### Security Group variables

variable "ingress-cidr-blocks" {
  default = ""
}

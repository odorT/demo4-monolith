resource "aws_db_subnet_group" "demo4-monolith" {
  name       = "demo4-monolith-db-subnet-group"
  subnet_ids = module.vpc.private_subnets

  tags = {
    "Name" = "demo4-monolith-db-subnet-group"
  }
}

resource "aws_db_parameter_group" "demo4-monolith" {
  name   = "demo4-monolith-db-pg"
  family = "mysql8.0"

  parameter {
    name  = "character_set_server"
    value = "utf8"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8"
  }
}

resource "aws_db_instance" "demo4-monolith-rds" {
  instance_class         = var.rds-type
  allocated_storage      = var.rds-storage
  max_allocated_storage  = var.rds-storage-max
  storage_type           = var.rds-storage_type
  engine                 = var.rds-engine
  engine_version         = var.rds-engine_version
  identifier             = var.identifier
  name                   = var.db_name
  username               = var.db_user
  password               = var.db_pass
  db_subnet_group_name   = aws_db_subnet_group.demo4-monolith.name
  vpc_security_group_ids = [aws_security_group.demo4-monolith.id]
  parameter_group_name   = aws_db_parameter_group.demo4-monolith.name
  publicly_accessible    = false
  skip_final_snapshot    = true
}

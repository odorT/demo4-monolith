region = "us-west-2"

vpc-cidr = "10.0.0.0/16"
vpc-priv-subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
vpc-pub-subnets = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

cluster_version = "1.20"

worker-name1 = "worker-group-1"
worker-type1 = "t2.small"
worker-asg-capacity1 = 2

worker-name2 = "worker-group-2"
worker-type2 = "t2.medium"
worker-asg-capacity2 = 1

rds-type = "db.t3.micro"
rds-storage = 50
rds-storage-max = 100
rds-storage_type = "gp2"
rds-engine = "mysql"
rds-engine_version = "8.0"

identifier = "demo4-monolith-rds"

ingress-cidr-blocks = [ "10.0.0.0/8", "185.32.44.0/24" ]

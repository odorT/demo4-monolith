provider "aws" {
  region = var.region
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.66.0"

  name                 = "demo4-eks-vpc"
  cidr                 = var.vpc-cidr
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = var.vpc-priv-subnets
  public_subnets       = var.vpc-pub-subnets
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_support   = true
  enable_dns_hostnames = true
}

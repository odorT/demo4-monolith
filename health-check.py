import requests
import time
import sys

url = f'http://{sys.argv[1]}/actuator/health'
print(url)

for i in range(20):
    response = str(requests.get(url))
    if "200" in response:
        print("------\n\nApplication is up and running\n\n------")
        break
    else:
        print(f"------\n\nCannot reach the given url: {url}\n\n------")
        time.sleep(10)
else:
    print("------\n\nApplication is down\n\n------")
    exit(1)

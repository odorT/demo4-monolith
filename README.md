[![pipeline status](https://gitlab.com/odorT/demo4-monolith/badges/master/pipeline.svg)](https://gitlab.com/odorT/demo4-monolith/-/commits/master)
---

## DEMO4-MONOLITH project
### This repo hosts the whole deployment configurations and scripts for [spring-petclinic](https://github.com/spring-projects/spring-petclinic) application using Infrastructure as Code (IAC) tools. Tools used in the project include:  
### - **AWS EKS**
### - **Terraform**
### - **Gitlab CI**
### - **Docker**

---
### Here are some notes to be considered before provisioning it:
- [app-folder](https://gitlab.com/odorT/demo4-monolith/-/tree/master/app-folder) folder contains application related configuration files and source code.
- [kubernetes](https://gitlab.com/odorT/demo4-monolith/-/tree/master/kubernetes) folder contains the kubernetes manifests for deployments. There are one deployment manifest for creating 3 replica pods running petclinic application and loadbalance service for exposing it to external connections, 2 secret manifests: one for gitlab(this is for pulling image from gitlab container registry), another for defining environment variables which is exported inside pods. There is also service account manifest for accessing kubernetes dashboard.  
    **Note**: Some definitions are starting TEMP_VALUE in [secrets.yaml](https://gitlab.com/odorT/demo4-monolith/-/blob/master/kubernetes/secrets.yaml) file, this is because the actual values are replaced in runtime. Check lines 166-170 in [.gitlab-ci.yml](https://gitlab.com/odorT/demo4-monolith/-/blob/master/.gitlab-ci.yml)
for better understanding
- Files in [terraform](https://gitlab.com/odorT/demo4-monolith/-/tree/master/terraform) folder are responsible for creating infrastructure in AWS. It saves the tfstate file in gitlab. When applied, it creates VPC, Security Groups, an RDS instance and EKS cluster with 3 nodes in `us-west-2` region. It can be changed in [variables.tf](https://gitlab.com/odorT/demo4-monolith/-/blob/master/terraform/variables.tf). Some variables are declared with null value, those will be overriden when applied with variables declared in CI/CD setting in gitlab.
- [Dockerfile](https://gitlab.com/odorT/demo4-monolith/-/blob/master/Dockerfile) is for dockerizing the application.  
  **Note**: The application jar file is generated in Gitlab runner, then copied to docker image. This is done for caching m2 repo between builds in order to achieve fast package generations. [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) is used here to build docker image.
- [.gitlab-ci.yml](https://gitlab.com/odorT/demo4-monolith/-/blob/master/.gitlab-ci.yml) The CI file for project. The pipeline is triggered when there is a new commit on project, but some jobs are only executed only changes on some spesific files(for more details, check .gitlab-ci.yml). When there is change in app-folder/ the whole pipeline is triggered, meaning that the new built application will be deployed to infrastructure. The last job - health-check sends request to check if the application is reachable or not. 

---
### For accesing kubernetes dashboard
1. Getting bearer token  
`kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/dashboard-admin -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"`

2. Creating tunnel with following command:  
`kubectl proxy`

3. Access WEB DASBHOARD UI by clicking this url:  
`http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/`

4. After finishing, remove SA and ClusterRoleBinding for security concerns.  
`kubectl -n kubernetes-dashboard delete serviceaccount admin-user`  
`kubectl -n kubernetes-dashboard delete clusterrolebinding admin-user`  


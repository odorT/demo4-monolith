FROM openjdk:8-jre-alpine3.9

ENV HOME /home/$USER
ARG USER=app

RUN apk add --update sudo
RUN adduser -D $USER && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER && chmod 0440 /etc/sudoers.d/$USER

USER $USER
WORKDIR $HOME
RUN sudo chown -R $USER:$USER $HOME

COPY *.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]
